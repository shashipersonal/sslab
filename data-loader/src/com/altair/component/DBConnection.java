package com.altair.component;

public class DBConnection {
	
	protected String dbAddress;
    protected int dbPort;
    protected String dbName;
    protected String dbPwd;
    protected String dbUser;
    protected String additionalProps;
    protected String protocol;
    protected String driver;
    protected String url;
    
	public String getDbAddress() {
		return dbAddress;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setDbAddress(String dbAddress) {
		this.dbAddress = dbAddress;
	}
	public int getDbPort() {
		return dbPort;
	}
	public void setDbPort(int dbPort) {
		this.dbPort = dbPort;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbPwd() {
		return dbPwd;
	}
	public void setDbPwd(String dbPwd) {
		this.dbPwd = dbPwd;
	}
	public String getDbUser() {
		return dbUser;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public String getAdditionalProps() {
		return additionalProps;
	}
	public void setAdditionalProps(String additionalProps) {
		this.additionalProps = additionalProps;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}

}
