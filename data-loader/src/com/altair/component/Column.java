package com.altair.component;

public class Column {
	
    public final static int DATATYPE_NONE           = 0;
    public final static int DATATYPE_STRING         = 1;
    public final static int DATATYPE_CLOB           = 2;
    public final static int DATATYPE_INT            = 3;
    public final static int DATATYPE_NUMERIC        = 4;
    public final static int DATATYPE_DATE           = 5;
    public final static int DATATYPE_TIME           = 6;
    public final static int DATATYPE_TIMESERIES     = 7; // DATE AND TIME
    public final static int DATATYPE_SNAPSHOT       = 8; // PROGRESSIVE (INCREMENTAL) WITH INHERENT "PROG" FUNCTION
    public final static int DATATYPE_GEOJSON		= 9;
    public final static int DATATYPE_BIGINT         = 10;

	
	private String name;
	
	private int dataType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	

}
