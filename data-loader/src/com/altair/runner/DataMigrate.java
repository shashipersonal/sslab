package com.altair.runner;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;

import org.ini4j.InvalidFileFormatException;

import com.altair.component.DBConnection;
import com.altair.connector.IDataConnector;
import com.altair.connector.factory.DataConnectorFactory;
import com.altair.loader.IDataLoader;
import com.altair.loader.factory.DataLoaderFactory;

public class DataMigrate {
	private static final org.ini4j.Ini iniObject  = new org.ini4j.Ini();
	
	static {
		//Load the config file
		File config = new File("loader.ini");
		iniObject.setFile(config);
		iniObject.getConfig().setLowerCaseOption(true);
		iniObject.getConfig().setLowerCaseSection(true);
		try {
			iniObject.load();
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws Exception 
	{		
		IDataConnector connector = null;
		IDataLoader loader = null;		
		List<String> skipList = Arrays.asList(new String[]{"source","destination","query"});
		
		try {
			//Establish connection to source
			String sourceType = iniObject.get("source", "type");
			connector = DataConnectorFactory.getConnector(sourceType);			
			DBConnection dbCon = getDBConnection("source");
			connector.init(dbCon);
			
			
			//Establish connection to Destination
			String destType = iniObject.get("destination", "type");
			loader = DataLoaderFactory.getLoader(destType);
			dbCon = getDBConnection("destination");		
			loader.intialize(dbCon);		
			
			for(String section: iniObject.keySet()) {
				if(!skipList.contains(section)) {
					String sourceSQL = iniObject.get(section, "sourcesql");
					String destTable = iniObject.get(section, "desttable");
					
					tableLoader(connector, loader, sourceSQL, destTable);					
				}
				else if(section.equalsIgnoreCase("query"))
				{
					String srcQuery=iniObject.get(section, "srcQuery");
					String destQuery=iniObject.get(section, "destQuery");

					if(srcQuery!=null )
					{
						//run src query
						connector.extractData(srcQuery);
					}//
					if(destQuery!=null) 
					{
						//run dest query
						loader.query(destQuery);
					}

				}
			}			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		finally {
			//close connections
			loader.close();
			connector.terminate();
		}
		
	}
	
	private static void tableLoader(IDataConnector connector,IDataLoader loader, 
			String sql, String table) throws Exception {
		ResultSet results = null;
		try {
			System.out.println("**************Running Migration***************");
			System.out.println("Source Query:"+sql);
			System.out.println("Destination Table:"+ table);
			
			//extract the data from source
			results = connector.extractData(sql);
			
			//load data to destination
			if(results != null) {
				System.out.println("Loading data:"+ table);
				loader.loadData(results,table);
			} else
				throw new Exception("No results obtained");
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			results.close();
			connector.cleanupResource();
			System.out.println("**********End of Migration for:"+table + "**********");
		}		
	}
	
	private static DBConnection getDBConnection(String identifier) throws Exception 
	{
		DBConnection dbCon = new DBConnection();		
		String val = iniObject.get(identifier, "host");
		
		if(val != null)
			dbCon.setDbAddress(val);
		else
			throw new Exception("Invalid "+ identifier + "details");
		
		 val = iniObject.get(identifier, "port");
		 if(val != null)
			 dbCon.setDbPort(Integer.parseInt(val));
		 
		 
		 val = iniObject.get(identifier, "username");
		 if(val != null)
			 dbCon.setDbUser(val);
		 else
			throw new Exception("Invalid "+ identifier + "details");
		 
		 val = iniObject.get(identifier, "pwd");
		 if(val != null)
			 dbCon.setDbPwd(val);
		 else
			throw new Exception("Invalid "+ identifier + "details");
		 
		 val = iniObject.get(identifier, "dbname");
		 if(val != null)
			 dbCon.setDbName(val);
		 else
			throw new Exception("Invalid "+ identifier + "details");
		 
		 val = iniObject.get(identifier, "additionalprops");
		 if(val != null)
			 dbCon.setAdditionalProps(val);
		
		return dbCon;
		
	}

}
