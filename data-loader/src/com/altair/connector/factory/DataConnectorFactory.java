package com.altair.connector.factory;

import com.altair.connector.IDataConnector;
import com.altair.connector.MonetDataConnector;
import com.altair.connector.MySQLDataConnector;
import com.altair.connector.SQLServerDataConnector;

public class DataConnectorFactory {
	
	public static IDataConnector getConnector(String type) {
		
		IDataConnector connector =null;
		switch (type) {
		case "sqlserver":
				connector = new SQLServerDataConnector();
			break;
		case "monetdb":
			connector = new MonetDataConnector();
			break;
		case "mysql":
			connector = new MySQLDataConnector();
			break;
		default:
			System.out.println("Connector Not Implemented");
			break;
		}
		
		return connector;
	}

}
