package com.altair.connector;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp.BasicDataSource;

import com.altair.component.DBConnection;
import com.altair.util.DatasourceUtil;

public class DataConnector implements IDataConnector{
	
	protected BasicDataSource ds = null;
	protected Connection con = null;
	
	@Override
	public void init(DBConnection con) throws Exception {		
		ds = DatasourceUtil.getConnection(con);					
	}
	
	@Override
	public ResultSet extractData(String sql) throws SQLException {
		con = ds.getConnection();
		ResultSet rset = null;
		try {
			
			// Run SQL Query and get result set
			rset = doQueryAndGetResult(con,sql);
			
			if(!rset.next())
			{
				throw new Exception("NO RECORDS");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} 
		
		return rset;
	}
	
	private ResultSet doQueryAndGetResult(Connection conn,String sql) throws Exception
	{
		
		DatabaseMetaData meta = conn.getMetaData();
		conn.setAutoCommit(false);
		String productName = meta.getDatabaseProductName();
		System.out.println("Connected to: "+productName);    
		
		//make Statement //NOTE this cannot be callable statement to support MonetDB also as a DS
		Statement cStmt=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);		
		cStmt.setQueryTimeout(600); //1 min
		
		//As per MySQL Doc, setFetchSize doesnt have effect other than toggle between full result set read
		//or strean line by line
		if(productName!=null && productName.equalsIgnoreCase("MySQL"))
			cStmt.setFetchSize(Integer.MIN_VALUE);
		else
			cStmt.setFetchSize(1000);

		return cStmt.executeQuery(sql);
	}//
	
	@Override
	public void cleanupResource() throws Exception {
		if(con!=null)
			con.close();		
	}
	
	@Override
	public void terminate() throws Exception {
		if(ds != null)
			ds.close();		
	}
}
