package com.altair.connector;

import com.altair.component.DBConnection;
import com.altair.util.DatasourceUtil;

public class MonetDataConnector extends DataConnector {
	
	@Override
	public void init(DBConnection con) throws Exception {
		
		String url = this.getDBUrl(con);
		con.setUrl(url);
		con.setDriver(this.getDriverName());
		
		ds = DatasourceUtil.getConnection(con);					
	}
	
	private String getDriverName() {
		return "nl.cwi.monetdb.jdbc.MonetDriver";
	}
	
	private String getDBUrl(DBConnection con) {
		String url = "jdbc:monetdb://"+con.getDbAddress();
		if(con.getDbPort() > 0) url += ":"+con.getDbPort();
		url += "/" + con.getDbName();
		if(con.getAdditionalProps() != null && !con.getAdditionalProps().isEmpty()) 
			url += ";"+con.getAdditionalProps();
		
		return url;
		
	}

}

