package com.altair.connector;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.altair.component.DBConnection;

public interface IDataConnector {
	
	public void init(DBConnection dbConn) throws Exception;
	
	public ResultSet extractData(String sql) throws SQLException;
	
	public void cleanupResource() throws Exception;
	
	public void terminate() throws Exception;

}
