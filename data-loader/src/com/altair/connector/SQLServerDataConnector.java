package com.altair.connector;

import com.altair.component.DBConnection;
import com.altair.util.DatasourceUtil;

public class SQLServerDataConnector extends DataConnector {
	
	@Override
	public void init(DBConnection con) throws Exception {
		
		String url = this.getDBUrl(con);
		con.setUrl(url);
		con.setDriver(this.getDriverName());
		
		ds = DatasourceUtil.getConnection(con);			
	}
	
	public String getDriverName() {
		return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	}
	
	public String getDBUrl(DBConnection con) {
		String url = "jdbc:sqlserver://"+con.getDbAddress();
		if(con.getDbPort() > 0) url += ":"+con.getDbPort();
		if(con.getDbName() !=null) url += ";database="+ con.getDbName();
		if(con.getAdditionalProps() != null) url += ";"+con.getAdditionalProps();
		return url;
		
	}

}
