package com.altair.util;

import org.apache.commons.dbcp.BasicDataSource;
import com.altair.component.DBConnection;

public class DatasourceUtil {
	/**
	 * Get connection
	 * @param con
	 * @return
	 */
	public static BasicDataSource getConnection(DBConnection con)
	{
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(con.getDriver());
		ds.setUrl(con.getUrl());
		ds.setUsername(con.getDbUser());
		ds.setPassword(con.getDbPwd());
		ds.setInitialSize(3);
		ds.setMaxActive(3);
		ds.setMaxIdle(6000); // 1 min

		return ds;
	}

    public static String escapeQuoutes(String str)
    {
    	str = str.replaceAll("([\"\\\\])", "\\\\$1");
        return str;
    }

}
