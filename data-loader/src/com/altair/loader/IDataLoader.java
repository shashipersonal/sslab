package com.altair.loader;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.altair.component.DBConnection;

public interface IDataLoader {
	public void intialize(DBConnection conn) throws Exception;
	public void loadData( ResultSet rset, String tableName) throws Exception;
	public void close() throws Exception;
	
	public ResultSet query(String sql) throws SQLException;
}
