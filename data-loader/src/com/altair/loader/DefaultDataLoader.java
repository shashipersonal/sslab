package com.altair.loader;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.dbcp.BasicDataSource;

import com.altair.component.Column;
import com.altair.component.DBConnection;
import com.altair.util.DatasourceUtil;

public class DefaultDataLoader implements IDataLoader{
	
	private static DateFormat theDateFormatDateOnly = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat theDateFormatTimeOnly = new SimpleDateFormat("HH:mm:ss");
    private static DateFormat theDateFormatDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    protected DBConnection dbCon = null;	
	protected BasicDataSource ds = null;
	
	protected static final String date2str(Date date, int format)
    {
        DateFormat df = null;
        
        switch(format) {
        case Column.DATATYPE_DATE:
            df = theDateFormatDateOnly;
            break;
        case Column.DATATYPE_TIME:
            df = theDateFormatTimeOnly;
            break;
        case Column.DATATYPE_SNAPSHOT:
        case Column.DATATYPE_TIMESERIES:
            df = theDateFormatDateTime;
            break;
        }
        
        String strVal = "";
        if (df != null) {
            synchronized(df.getClass()) {
                strVal = df.format(date);
            }
        }
        
        return strVal;
    }
	
	protected String getDataType_ba2db(int dataType)
	{
		String name = null;
		switch (dataType)
		{
		case Column.DATATYPE_INT:
			name = "INT";
			break;
		case Column.DATATYPE_BIGINT:
			name = "BIGINT";
			break;
		case Column.DATATYPE_NUMERIC:
			name = "DECIMAL(18,5)";
			break;
		case Column.DATATYPE_DATE:
			name = "DATE";
			break;
		case Column.DATATYPE_TIMESERIES:
		case Column.DATATYPE_SNAPSHOT:
			name = "TIMESTAMP";
			break;
		case Column.DATATYPE_TIME:
			name = "TIME";
			break;
		case Column.DATATYPE_STRING:
		default:
			name = "VARCHAR(256)";
			break;
		case Column.DATATYPE_CLOB:
			name = "CLOB";
			break;
		}
		return name;
	}

	@Override
	public void intialize(DBConnection conn) throws Exception {
		// TODO Auto-generated method stub

		this.dbCon = conn;
		
	}

	@Override
	public void loadData(ResultSet rset, String tableName) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	protected final String quote(String dimName, boolean bParseDots)
	{
		StringBuilder sqlName = new StringBuilder();
		if (bParseDots)
		{
			for (String dd : dimName.split("\\."))
			{
				if (sqlName.length() > 0) sqlName.append(".");
				sqlName.append(quote(dd));
			}
		}
		else 
			sqlName.append(quote(dimName));

		return sqlName.toString();
	}

	protected String quote(String colName)
	{
		colName = colName.replaceAll("\"", "&quot;");
		return "\"" + colName + "\"";
	}

	protected String quoteFtable(String ftable, boolean bParseDots){
		return quote(ftable, bParseDots);
	}
	
	protected String getCreateSQL(Connection con, Column cols[], String table) throws Exception 
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append("CREATE TABLE " + quoteFtable(table, true) + "(");
		
		String prefix = "";
		for(Column col:cols) {
			sql.append(prefix);
			prefix = ",";
			sql.append(quote(col.getName(),false)).append(" ").append(getDataType_ba2db(col.getDataType()));			
		}
		
		sql.append(")");
		
		return sql.toString();
	}

	
	public Column[] getColumnSchemas(ResultSet rset) throws Exception
	{		
		Column[] retValue=null;
		
		if(rset==null)
			throw new Exception("Cannot extract column information. Result set is null");
		
		//get meta data
		ResultSetMetaData rsetMetaData= rset.getMetaData();
		
		//get number of cols and make the arry of BADimension
		int numberOfColumns = rsetMetaData.getColumnCount();
		retValue=new Column[numberOfColumns];
		
		//loop thru the cols and fill in BADimension with name,label and data type.
		for(int i=0;i<numberOfColumns;i++)
		{			
			retValue[i]=new Column();
			//Hack to remove the quotes
			//When we load a CSV with header having 'Hospital Name' dimName and will be 'Hospital Name'
			// but in the table the column will create as '"Hospital Name"'. While comparing the dimName with 
			// actual column name, it does not match
			String _newDimName = rsetMetaData.getColumnLabel(i+1).trim();
			if(_newDimName!=null && _newDimName.startsWith("\"") && _newDimName.endsWith("\""))
				_newDimName = _newDimName.substring(1,_newDimName.length()-1);
			retValue[i].setName(_newDimName);	//fix for VA-733, if we have alias for column then use it for dimName			
			retValue[i].setDataType(getDimTypeFromDBType(rsetMetaData.getColumnType(i+1)));						
		}//
				
		return retValue;			
	}//
	
	public int getDimTypeFromDBType(int nSQLType) throws Exception
	{
		int nRetVal=Column.DATATYPE_NONE;
		
		if(nSQLType==Types.INTEGER || nSQLType==Types.TINYINT ||
				nSQLType==Types.SMALLINT || nSQLType==Types.BIT || nSQLType==Types.ROWID || nSQLType==Types.DISTINCT)
		{
			nRetVal= Column.DATATYPE_INT;
		}
		else if(nSQLType==Types.BIGINT) 
		{
			nRetVal= Column.DATATYPE_BIGINT;
		}
		else if(nSQLType==Types.FLOAT || nSQLType==Types.DECIMAL || nSQLType==Types.DOUBLE ||
				nSQLType==Types.NUMERIC || nSQLType==Types.REAL)
		{
			nRetVal= Column.DATATYPE_NUMERIC;
		}
		else if(nSQLType==Types.DATE)
		{
			nRetVal= Column.DATATYPE_DATE;
		}
		else if(nSQLType==Types.TIME )
		{
			nRetVal= Column.DATATYPE_TIME;
		}//
		else if(nSQLType==Types.TIMESTAMP )
		{
			nRetVal= Column.DATATYPE_TIMESERIES;
		}//
		else if(nSQLType==Types.CLOB || nSQLType==Types.BLOB || nSQLType==Types.NCLOB)
		{
			nRetVal= Column.DATATYPE_CLOB;
		}
		else if(nSQLType==Types.VARCHAR || nSQLType==Types.CHAR || nSQLType==Types.NCHAR
			|| nSQLType==Types.NVARCHAR || nSQLType==Types.LONGVARCHAR || nSQLType==Types.LONGNVARCHAR ||
			nSQLType==Types.SQLXML)
		{
			nRetVal= Column.DATATYPE_STRING;
		}
		else if(nSQLType==Types.BOOLEAN)
		{
			nRetVal= Column.DATATYPE_INT;
		}
		else
		{
			nRetVal= Column.DATATYPE_NONE;
		}//
						
		return nRetVal;
	}//
	
		
	public Object[] getValuesForRecord(Column[] dims,ResultSet rset) throws Exception
	{		
		Object[] arrValues=new Object[dims.length];
		
		arrValues=new Object[dims.length];
		for(int i=0;i<dims.length;i++)
		{
			Object objFromDB=rset.getObject(i+1);
			
			if(objFromDB==null || objFromDB.toString().trim().length()==0)	{
				continue;
			}
			else if(objFromDB instanceof Boolean)
			{
				Boolean b=(Boolean) objFromDB;
				if(b.booleanValue())
				{
					arrValues[i]=new Integer(1);
				}
				else
				{
					arrValues[i]=new Integer(0);
				}//
				
			}//
			else if(dims[i].getDataType()==Column.DATATYPE_DATE)
			{				
				/*try {
				arrValues[i]= theDateFormatDateOnly.parse(objFromDB.toString());
				} catch(Exception ex) {
					ex.printStackTrace();
				}*/
				arrValues[i] = objFromDB.toString(); 
			}
			else if(dims[i].getDataType()==Column.DATATYPE_TIME)
            {  
			    if (objFromDB instanceof java.util.Date || objFromDB instanceof java.sql.Date) // Happens with SQLServer
			        arrValues[i] = objFromDB.toString();               
            }
			else if(dims[i].getDataType() == Column.DATATYPE_STRING)
			{
				arrValues[i]=DatasourceUtil.escapeQuoutes(objFromDB.toString());					
			}
			else
			{
				arrValues[i] = objFromDB;
			}				
			
		}//
		
		return arrValues;
	}//

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public ResultSet query(String sql) throws SQLException {
		Connection con = ds.getConnection();
		ResultSet rset = null;
		try {
			
			// Run SQL Query and get result set
			rset = doQueryAndGetResult(con,sql);
			
			if(!rset.next())
			{
				throw new Exception("NO RECORDS");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} 
		con.close();
		return rset;
	}
	
	private ResultSet doQueryAndGetResult(Connection conn,String sql) throws Exception
	{
		
		DatabaseMetaData meta = conn.getMetaData();
		conn.setAutoCommit(false);
		String productName = meta.getDatabaseProductName();
		System.out.println("Connected to: "+productName);    
		
		//make Statement //NOTE this cannot be callable statement to support MonetDB also as a DS
		Statement cStmt=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);		
		cStmt.setQueryTimeout(600); //1 min
		
		//As per MySQL Doc, setFetchSize doesnt have effect other than toggle between full result set read
		//or strean line by line
		if(productName!=null && productName.equalsIgnoreCase("MySQL"))
			cStmt.setFetchSize(Integer.MIN_VALUE);
		else
			cStmt.setFetchSize(1000);

		return cStmt.executeQuery(sql);
	}//
	
}
