package com.altair.loader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import nl.cwi.monetdb.mcl.io.BufferedMCLReader;
import nl.cwi.monetdb.mcl.io.BufferedMCLWriter;
import nl.cwi.monetdb.mcl.net.MapiSocket;

import com.altair.component.Column;
import com.altair.component.DBConnection; 
import com.altair.util.DatasourceUtil;


public class MonetDataLoader extends DefaultDataLoader{
	

	@Override
	public void intialize(DBConnection conn) throws Exception 
	{
		
		super.intialize(conn);
		conn.setDriver(getDriverName());
		conn.setUrl(getDBUrl(conn));
		
		ds = DatasourceUtil.getConnection(dbCon);

	}
	
	@Override
	public void loadData(ResultSet rset, String tableName) throws Exception {
		Connection con = ds.getConnection();
		Column[] cols = getColumnSchemas(rset);
		Statement stmt = con.createStatement();
		
		String sql = getCreateSQL(con, cols,tableName);
		System.out.println("QUERY:"+sql);

		stmt.executeUpdate(sql);
		System.out.println("Successfully created table:"+ sql);
		stmt.close();
		con.close();
		
		IBulkLoader loader = new BATransaction_MonetDB(tableName);
		loader.initialize(cols);
		loadResults(cols, rset, loader);
		loader.commit();
	}
	
	private String getDriverName() {
		return "nl.cwi.monetdb.jdbc.MonetDriver";
	}
	
	private String getDBUrl(DBConnection con) {
		String url = "jdbc:monetdb://"+con.getDbAddress();
		if(con.getDbPort() > 0) url += ":"+con.getDbPort();
		url += "/" + con.getDbName();
		if(con.getAdditionalProps() != null && !con.getAdditionalProps().isEmpty()) 
			url += ";"+con.getAdditionalProps();
		return url;
		
	}
	
	protected void loadResults(Column[] dims, ResultSet rset,IBulkLoader loader) throws Exception
	{		
		int queueSize = 1000000;
        
        //queue to hold the records read from the Resultset
		final BlockingQueue<Record_RDBMS> queue = new LinkedBlockingQueue<>(queueSize);
		final Boolean[] bSuccess = new Boolean[] {true};
		final StringBuilder rootCauses = new StringBuilder();
        
		// Load the rest of the data using multi cores
		int nProc = Runtime.getRuntime().availableProcessors()-1;
        final int nThreads = nProc < 1 ? 1: nProc;
        
		Thread.UncaughtExceptionHandler exceptionHandler = new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread th, Throwable ex) {
                bSuccess[0] = false;
                
                while(ex.getCause()!=null)
					ex=ex.getCause();
				
                rootCauses.append(ex.getMessage()+"\\n");
                queue.clear();
                
                for (int i=0; i<nThreads; ++i)
                    try {
                        queue.put(new Record_RDBMS(true, null));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        };            
        
        List<Thread> threads = new ArrayList<Thread>();
        String threadName = "BADataLoader_RDBMS-";
        for (int i=0; i<nThreads; ++i)
        {
        	Thread thread = new DataLoader_RDBMS(queue, loader,3000); 
            thread.setUncaughtExceptionHandler(exceptionHandler);
            threads.add(thread);
            thread.setName(threadName+i);
            thread.start();
        }
        
		//go through each row in result set and get values from DefaultRDBMSDataExtractor		
		do
		{
			if(bSuccess[0] == false)
				break;

			Object[]  arrValues = getValuesForRecord(dims, rset);
			queue.put(new Record_RDBMS(false, arrValues));
			
		}while(rset.next());
		
		//End of the Resultset signal
		for (int i=0; i<nThreads; ++i)
			queue.put(new Record_RDBMS(true, null));
		
		 // Wait for all the loader threads       
        for (Thread thread:threads)
          thread.join();
        
        if (bSuccess[0] == false)
            throw new Exception(rootCauses.toString());        
	}//


	@Override
	public void close() throws Exception {		
		ds.close();
	}

	private interface IBulkLoader {
		
		public void initialize(Column[] dims) throws Exception;
		public void insertData(Object[] values) throws Exception;
		public void commit() throws Exception;

	}
	////////////////////////////////////////////
	private class BATransaction_MonetDB implements IBulkLoader{

		private String ftable;
		private MapiSocket server = null;		
		private volatile int insertedLines = 0;
		private int blockSize = 100000;
		private Column columns[] = null;
		
		public BATransaction_MonetDB(String ftable) throws Exception 
		{
			this.ftable = ftable;
			startTransaction();
				
		}
		
		public void initialize(Column[] dims) throws Exception
		{
		    this.columns = dims;
		}
		

		private void startTransaction() throws Exception {
			// Create a MapiSocket and connect to it
			if (this.server == null)
			{
				this.server = new MapiSocket();
				this.server.setDatabase(dbCon.getDbName());
				this.server.setLanguage("sql");
				this.server.connect(dbCon.getDbAddress(), dbCon.getDbPort(), dbCon.getDbUser(), dbCon.getDbPwd());
				this.server.setSoTimeout(0);

				BufferedMCLReader in  = this.server.getReader();
				BufferedMCLWriter out = this.server.getWriter();

				String error = in.waitForPrompt();
				if (error != null)
					throw new Exception(error);

				// The leading 'S" and trailing semi-colon are essential, since it is a protocol marker
				String sql = "sCOPY INTO " + quote(ftable,true) + " FROM STDIN USING DELIMITERS ',','\n', '\"' NULL AS '';";
				out.write(sql); 
				out.newLine();
				insertedLines = 0;
			}
		}


		public void insertData(Object[] values) throws Exception
		{
			StringBuilder str = new StringBuilder();
			String strVal;
			for (int i = 0; i < values.length; ++i)
			{
				if (i > 0) str.append(",");

				if (values[i] == null)
					str.append("");
				else if (values[i] instanceof Date)
					str.append(date2str((Date)values[i], columns[i].getDataType()));
				else if (values[i] instanceof String) 
				{
					strVal = (String) values[i];
					strVal = DatasourceUtil.escapeQuoutes(strVal);
					if(strVal.length() > 256) 
						strVal = strVal.substring(0, 256).replaceAll("[\\\\]*$", ""); //VA-1607 trailing character shouldn't be a escape char
					str.append("\"" + strVal + "\"");
				}
				else // Numeric
					str.append(values[i].toString());
			}
			
			synchronized(this) {
				insertRecord(str);   
			}
		}

		public void insertRecord(StringBuilder str) throws Exception
		{
			BufferedMCLWriter out = this.server.getWriter();
			out.write(str.toString());
			out.newLine();

			insertedLines++;
			if(insertedLines % this.blockSize == 0) {

				//commit Transaction
				this.commit();

				//Initialize new transaction again
				this.startTransaction();
			}
		}

		
		public void commit() throws Exception
		{
			if (this.server != null)
			{
				BufferedMCLReader in  = this.server.getReader();
				BufferedMCLWriter out = this.server.getWriter();
				String msg = null;
				StringBuilder errorMsgs = new StringBuilder(); 
				try {
					out.writeLine(""); //Need this for synchronization over flush()

					msg = in.waitForPrompt();
					// Fix for VA-2788.
					// MonetDB error messages may start with "!" or a numeric code
					// followed by "!" (eg. 22000!). Using a regex to detect both 
					// classes correctly.
					// Added by - Hasim K
					if (msg!=null && msg.matches("(\\d)*!.*"))
						errorMsgs.append(msg+"\n");
					if (errorMsgs.length() > 0){
						throw new Exception(errorMsgs.toString());
					}    				

					// Tell the server "Done" and ask it to commit
					out.writeLine(""); //Server wants more.  We are going to tell it "That is it!"                
					msg=null;
					while ( !(msg=in.readLine()).equals(".") )
						if (msg.startsWith("!"))
							errorMsgs.append(msg+"\n");
					in.waitForPrompt();    

					if (errorMsgs.length() > 0) {
						throw new Exception(errorMsgs.toString());
					}
				} finally {
					// Disconnect from server
					out.close();
					this.server.close();
					this.server = null;
				}
			}
		}
	}
	
	public class DataLoader_RDBMS extends Thread
	{
	    private BlockingQueue<Record_RDBMS> queue;
	    private int etlTimeOut;
	    private IBulkLoader loader;
	    public DataLoader_RDBMS(BlockingQueue<Record_RDBMS> queue, IBulkLoader loader,
	            int etlTimeOut)
	    {
	        this.queue = queue;
	        this.etlTimeOut = etlTimeOut;
	        this.loader = loader;
	    }

	    @Override
	    public void run()
	    {
	        try
	        {
	            Record_RDBMS record = null;
	            while(true)
	            {
	                record = queue.poll(etlTimeOut, TimeUnit.SECONDS);
	                if (record == null) throw new Exception("ETL_TIMEOUT");
	                
	                //Break the loop if we reach the end of ResultSet
	                if (record.isEndOfDs()) break;
	                
	                //insert actual values to dataEngine by obtaining it from wrapper
	                loader.insertData(record.getValues());
	            }
	                
	        }
	        catch (Exception ex)
	        {
	        	throw new RuntimeException(ex);
	        }
	    }
	}
	
	public class Record_RDBMS {
		private boolean endOfDs;
		private Object[] values;
		
		public Record_RDBMS(boolean status,Object[] values) {
			this.endOfDs = status;
			this.values = values;
		}
		public boolean isEndOfDs() {
			return endOfDs;
		}
		public Object[] getValues() {
			return values;
		}
		
	}


}
