package com.altair.loader.factory;


import com.altair.loader.IDataLoader;
import com.altair.loader.MonetDataLoader;

public class DataLoaderFactory {
	
	public static IDataLoader getLoader(String type) {
		
		IDataLoader loader =null;
		switch (type) {
		case "monetdb":
			loader = new MonetDataLoader();
			break;

		default:
			System.out.println("Loader Not Implemented");
			break;
		}
		
		return loader;
	}

}