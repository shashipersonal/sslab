Command to Execute:
-------------------

>java -cp ./dataloader.jar;ini4j-0.5.1.jar;commons-dbcp-1.4.jar;commons-pool-1.6.jar;monetdb-jdbc-2.8.jar;org.springframework.core-3.1.2.RELEASE.jar;org.springframework.jdbc-3.1.2.RELEASE.jar;org.springframework.transaction-3.1.2.RELEASE.jar;sqljdbc4.jar com.altair.runner.DataMigrate


About config file:
------------------

Below are the sections listed in the config file

[source] - DB connection details for the source database from where the data has to be migrated

[destination] - DB to which the data has to be pumped

[TABLE*] - User can specify as many [Table] sections with 2 attributes

1. sourcesql = Utility will run the specified query and captures the result
2. destinationTable = utility will load the above results in to the destination DB with the sepcified table name.

[Query] - In this section user can define a query to be run against the Destination DB, some thing like ALTER/UPDATE/DELETE etc.
It will use the already established connection to the destination DB and executes the user specified query.